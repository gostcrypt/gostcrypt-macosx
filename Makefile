#----- Command line arguments ------
# DEBUG:		enable optimizations and enable debugging checks
# DEBUGGER:		Enable debugging information for use by debuggers
# NOASM:		Exclude modules requiring assembler
# NOGUI:		enable graphical user interface (build console-only application)
# NOSTRIP:		Do not strip release binary
# NOTEST:		Do not test release binary
# RESOURCEDIR:	Run-time resource directory
# VERBOSE:		Enable verbose messages
# WXSTATIC:		Use static wxWidgets library

#------ Targets ------
# all
# clean
# wxbuild:		Configure and build wxWidgets - source code must be located at $(WX_ROOT)


#------ Build configuration ------

export APPNAME := gostcrypt
export BASE_DIR := $(CURDIR)
export BUILD_INC := $(BASE_DIR)/Build/Include

export AR ?= ar
export CC ?= gcc
export CXX ?= g++
export AS := nasm
export RANLIB ?= ranlib

export CFLAGS := -Wall
export CXXFLAGS := -Wall -Wno-unused-parameter
C_CXX_FLAGS := -MMD -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGE_FILES -I$(BASE_DIR) -I$(BASE_DIR)/Crypto -O2
export ASFLAGS := -Ox -D __GNUC__
export LFLAGS :=

export PKG_CONFIG_PATH ?= /usr/local/lib/pkgconfig

export WX_CONFIG ?= wx-config
export WX_CONFIG_ARGS := --unicode
WX_CONFIGURE_FLAGS :=
export WXCONFIG_CFLAGS := `wx-config --cflags`
export WXCONFIG_CXXFLAGS := `wx-config --cxxflags`
WX_ROOT ?= /Users/cvo/Documents/GostCrypt/GostCrypt_MacOSX_1.0/wxWidgets-3.0.1


export GST_BUILD_CONFIG := Release

ifeq "$(origin DEBUG)" "command line"
	ifneq "$(DEBUG)" "0"
		GST_BUILD_CONFIG := Debug
	endif
endif

ifeq "$(origin NOGUI)" "command line"
	export GST_NO_GUI := 1
	C_CXX_FLAGS += -DGST_NO_GUI
	WX_CONFIGURE_FLAGS += --enable-gui
endif

ifeq "$(origin PKCS11_INC)" "command line"
	C_CXX_FLAGS += -I$(PKCS11_INC)
endif

ifeq "$(origin RESOURCEDIR)" "command line"
	C_CXX_FLAGS += -DGST_RESOURCE_DIR="$(RESOURCEDIR)"
endif

ifneq "$(origin VERBOSE)" "command line"
	MAKEFLAGS += -s
endif

ifeq "$(origin WXSTATIC)" "command line"
	WX_CONFIG = $(WX_BUILD_DIR)/wx-config
	WX_CONFIG_ARGS += --static
endif


#------ Release configuration ------

ifeq "$(GST_BUILD_CONFIG)" "Release"

	C_CXX_FLAGS += -O2 -fno-strict-aliasing  # Do not enable strict aliasing
	export WX_BUILD_DIR ?= $(BASE_DIR)/wxrelease
	WX_CONFIGURE_FLAGS += --enable-debug_flag --enable-debug_gdb --enable-debug_info

else

#------ Debug configuration ------

	C_CXX_FLAGS += -DDEBUG
	CXXFLAGS += -fno-default-inline -Wno-unused-function -Wno-unused-variable
	export WX_BUILD_DIR ?= $(BASE_DIR)/wxdebug
	WX_CONFIGURE_FLAGS += --enable-debug_flag --enable-debug_gdb --enable-debug_info

endif


#------ Debugger configuration ------

ifeq "$(origin DEBUGGER)" "command line"

	C_CXX_FLAGS += -ggdb  
	WX_CONFIGURE_FLAGS += --enable-debug_gdb --enable-debug_info

endif


#------ Platform configuration ------

export PLATFORM := "Unknown"
export PLATFORM_UNSUPPORTED := 0

export CPU_ARCH ?= x64

ARCH = $(shell uname -p)
ifeq "$(ARCH)" "unknown"
	ARCH = $(shell uname -m)
endif

ifneq (,$(filter i386 i486 i586 i686 x86,$(ARCH)))
	CPU_ARCH = x86
	ASM_OBJ_FORMAT = elf32
else ifneq (,$(filter x86_64 x86-64 amd64 x64,$(ARCH)))
	CPU_ARCH = x64
	ASM_OBJ_FORMAT = elf64
endif

ifeq "$(origin NOASM)" "command line"
	CPU_ARCH = unknown
endif

ifeq "$(CPU_ARCH)" "x86"
	C_CXX_FLAGS += -D GST_ARCH_X86
else ifeq "$(CPU_ARCH)" "x64"
	C_CXX_FLAGS += -D GST_ARCH_X64
endif


#------ Linux configuration ------

ifeq "$(shell uname -s)" "Linux"

	PLATFORM := Linux
	C_CXX_FLAGS += -DGST_UNIX -DGST_LINUX

	ifeq "$(GST_BUILD_CONFIG)" "Release"
		C_CXX_FLAGS += -fdata-sections -ffunction-sections
		LFLAGS += -Wl,--gc-sections

		ifneq "$(shell ld --help 2>&1 | grep sysv | wc -l)" "0"
			LFLAGS += -Wl,--hash-style=sysv
		endif

		WXCONFIG_CFLAGS += -fdata-sections -ffunction-sections
		WXCONFIG_CXXFLAGS += -fdata-sections -ffunction-sections
	endif

endif


#------ Mac OS X configuration ------

ifeq "$(shell uname -s)" "Darwin"

	PLATFORM := MacOSX
	APPNAME := GostCrypt

	GST_OSX_SDK ?= /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk
	CC := gcc
	CXX := g++

	C_CXX_FLAGS += -DGST_UNIX -DGST_BSD -DGST_MACOSX -mmacosx-version-min=10.9 -isysroot $(GST_OSX_SDK) -D_XOPEN_SOURCE
	LFLAGS += -mmacosx-version-min=10.9 -Wl,-syslibroot $(GST_OSX_SDK)
	WX_CONFIGURE_FLAGS += --with-macosx-version-min=10.9 --with-macosx-sdk=$(GST_OSX_SDK)

	ifeq "$(CPU_ARCH)" "x64"
		CPU_ARCH = x86
	endif

	ASM_OBJ_FORMAT = macho
	ASFLAGS += --prefix _

	ifeq "$(GST_BUILD_CONFIG)" "Release"

		export enable_PRECOMPILED_HEADERS := 1

		S := $(C_CXX_FLAGS)
		C_CXX_FLAGS = $(subst -MMD,,$(S))

		C_CXX_FLAGS += -gfull 
		LFLAGS += -Wl,-dead_strip 

#		WX_CONFIGURE_FLAGS += --enable-universal_binary
		WXCONFIG_CFLAGS += -gfull
		WXCONFIG_CXXFLAGS += -gfull

	else

		WX_CONFIGURE_FLAGS += --enable-universal_binary

	endif

endif


#------ FreeBSD configuration ------

ifeq "$(shell uname -s)" "FreeBSD"

	PLATFORM := FreeBSD
	PLATFORM_UNSUPPORTED := 1
	C_CXX_FLAGS += -DGST_UNIX -DGST_BSD -DGST_FREEBSD

endif


#------ Solaris configuration ------

ifeq "$(shell uname -s)" "SunOS"

	PLATFORM := Solaris
	PLATFORM_UNSUPPORTED := 1
	C_CXX_FLAGS += -DGST_UNIX -DGST_SOLARIS
	WX_CONFIGURE_FLAGS += --with-gtk

endif


#------ Common configuration ------

CFLAGS := $(C_CXX_FLAGS) $(CFLAGS) $(GST_EXTRA_CFLAGS)
CXXFLAGS := $(C_CXX_FLAGS) $(CXXFLAGS) $(GST_EXTRA_CXXFLAGS)
ASFLAGS += -f $(ASM_OBJ_FORMAT)
LFLAGS := $(LFLAGS) $(GST_EXTRA_LFLAGS)

WX_CONFIGURE_FLAGS += --with-libjpeg --with-libtiff --with-libpng --with-zlib --with-opengl --with-cocoa --without-sdl \
--enable-aui --disable-sdltest --enable-unicode --disable-display \
--enable-xrc --enable-graphics_ctx --enable-monolithic --with-macosx-version-min=10.9 \
--enable-stl --enable-std_containers --enable-std_iostreams --enable-std_string \
--enable-std_string_conv_in_wxstring --disable-shared \



#------ Project build ------

PROJ_DIRS := Platform Volume Driver/Fuse Core Main

.PHONY: all clean wxbuild usage help

all clean:
	@if pwd | grep -q ' '; then echo 'Error: source code is stored in a path containing spaces' >&2; exit 1; fi

	@for DIR in $(PROJ_DIRS); do \
		PROJ=$$(echo $$DIR | cut -d/ -f1); \
		$(MAKE) -C $$DIR -f $$PROJ.make NAME=$$PROJ $(MAKECMDGOALS) || exit $?; \
		export LIBS="$(BASE_DIR)/$$DIR/$$PROJ.a $$LIBS"; \
	done	


#------ wxWidgets build ------

ifeq "$(MAKECMDGOALS)" "wxbuild"
CFLAGS :=
CXXFLAGS :=
LFLAGS :=
endif

wxbuild:

ifneq "$(shell test -f $(WX_ROOT)/configure || test -f $(WX_BUILD_DIR)/../configure && echo 1)" "1"
	@echo 'Error: WX_ROOT must point to wxWidgets source code directory' >&2
	@exit 1
endif

	rm -rf "$(WX_BUILD_DIR)"
	mkdir -p "$(WX_BUILD_DIR)"
	@echo Configuring wxWidgets library...
	cd "$(WX_BUILD_DIR)" && "$(WX_ROOT)/configure" $(WX_CONFIGURE_FLAGS) >/dev/null
	
	@echo Building wxWidgets library...
	cd "$(WX_BUILD_DIR)" && $(MAKE)

# ----- Help ---------------- 
usage: help
help:
	@echo "############################################################"
	@echo "#                                                          #"
	@echo "# For more information, please go on www.gostcrypt.org and #"
	@echo "#               Choose the wiki link                       #"
	@echo "#       Or join us on the freenode #gostcrypt channel      #"
	@echo "#                                                          #"
	@echo "############################################################"
	@echo ""
	@echo "There are the different options you can use with the Makefile"
	@echo ""
	@echo ""
	@echo "VERBOSE=1 : print informations of compilation"
	@echo "DEBUG=1 : Disable optimizations and enable debugging checks"
	@echo "DEBUGGER=1 : Enable debugging information for use by debuggers"
	@echo "NOASM=1 : Exclude modules requiring assembler"
	@echo "NOGUI=1 : Disable graphical user interface (build console-only application)"
	@echo "NOSTRIP=1 : Do not strip releasy binary"
	@echo "NOTEST=1 : Do not test release binary"
	@echo "RESOURCEDIR=/path : Run-time resource directory"
	@echo "WXSTATIC=1 : Use static wxWidgets library"
	@echo "PKCS11_INC=/path : Path to the PKCS11 headers"

